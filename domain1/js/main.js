(function () {
    var resizeWidth = 0 // Resize width for new widget
    var arrOfIframe = [] // Array of widgets
    var domain = 'https://domain2.000webhostapp.com'
    var frameId
    var color

    // Listeners of events
    document.getElementById('add_iframe').addEventListener('click', handleButtonClick, false)
    document.getElementById('btn_go').addEventListener('click', handleButtonGoClick, false)
    window.addEventListener('message', function (event) {
        if (~event.origin.indexOf(domain)) {
            if (event.data.action === 'resize') {
                arrOfIframe[event.data.key].style.height = event.data.value + 15 + 'px'
            } else if (event.data.action === 'renew') {
                arrOfIframe.forEach(function (i, k) {
                    sendRequest(i, {
                        action: 'renew',
                        value: event.data.value,
                        key: k
                    })
                })
            }
        }
    })

    /**
     * Add new widget on page
     * 
     * @param {object} e
     * @returns {Boolean}
     */
    function handleButtonClick(e) {
        var block = prepareFrame()
        document.getElementById('grid').insertAdjacentHTML('beforeend', block)
        document.getElementById(frameId + color).addEventListener('load', resizeIframe, false)

        return true;
    }

    /**
     * Searching and selecting text in random widget
     * 
     * @param {object} e
     * @returns {Boolean}
     */
    function handleButtonGoClick(e) {
        if (arrOfIframe.length === 0) {
            return false;
        }

        sendRequest(arrOfIframe[getRandomNumber(0, arrOfIframe.length)], {
            action: 'search',
            value: document.getElementById('search').value
        })

        return true;
    }

    /**
     * Change size of widget and attach listener
     * 
     * @param {object} obj
     * @returns {Boolean}
     */
    function resizeIframe(obj) {
        obj = obj.path[0]
        arrOfIframe.push(obj)

        sendRequest(obj, {
            action: 'resize',
            value: resizeWidth,
            key: arrOfIframe.length - 1
        })

        return true;
    }

    /**
     * Prepares frame for inserting on page
     * 
     * @returns {String}
     */
    function prepareFrame() {
        var pattern = '<iframe class="frame" id="$[id]" src="$[domain]" style="width: $[width]px; border-color: $[bColor];"  scrolling="no">'
        color = getRandomColor()
        frameId = getRandomNumber(1, 9999)        
        resizeWidth = getRandomNumber(100, 500) + 10

        pattern = pattern.replace('$[width]', resizeWidth).replace('$[bColor]', color).replace('$[id]',frameId + color).replace('$[domain]',domain)

        return pattern
    }

    /**
     * Change value of all widgets on page
     * 
     * @param {object} e 
     * @returns {Boolean}
     */
    function handleTextareaChange(e) {
        var text = this.value
        arrOfIframe.forEach(function (item) {
            item.contentWindow.document.getElementById('textarea').value = text
        });

        return true;
    }

    /**
     * Function generates random color for border 
     * @returns {String}
     */
    function getRandomColor() {
        var i
        var colorHex = '#';
        var letters = '0123456789ABCDEF'
        for (i = 0; i < 6; i++) {
            colorHex += letters[~~(Math.random() * 16)];
        }

        return colorHex;
    }

    /**
     * Generate random width of widget
     * 
     * @returns {Number}
     */
    function getRandomNumber(min, max) {

        return ~~(Math.random() * (max - min)) + min
    }

    /**
     * Send request to domain2
     * 
     * @param {object} obj
     * @param {json} data
     * @returns {Boolean}
     */
    function sendRequest(obj, data) {
        obj.contentWindow.postMessage(data, domain)

        return true
    }
})()