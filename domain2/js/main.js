(function () {
    var domain1 // Source of domain1
    var domain = 'https://vlasyuk94.000webhostapp.com'

// Listeners
    document.getElementById('textarea').addEventListener('keyup', handleTextareaKeyup, false)
    window.addEventListener('message', function (event) {
        var start
        var textarea
        var request = false;

        if (~event.origin.indexOf(domain)) {
            domain1 = event.source
            textarea = document.getElementById('textarea')
            if (event.data.action === 'resize') {
                textarea.style.width = event.data.value - 15 + 'px'
                request = resize(event, textarea)
            } else if (event.data.action === 'renew') {
                textarea.value = event.data.value
                event.data.value = parseInt(textarea.style.width.replace('px', ''))
                request = resize(event, textarea)
            } else if (event.data.action === 'search') {
                start = textarea.value.indexOf(event.data.value)
                textarea.focus();
                textarea.selectionStart = start;
                textarea.selectionEnd = start + event.data.value.length;
            }
            domain1.postMessage(request, event.origin)
        }
    });

    /**
     * Resize textarea by request
     * 
     * @param {object} event
     * @param {object} textarea
     * @returns {json}
     */
    function resize(event, textarea) {
        textarea.style.height = '1px'
        textarea.style.height = (textarea.scrollHeight - 4) + 'px'

        return {
            action: 'resize',
            value: textarea.scrollHeight - 4,
            key: event.data.key
        }
    }


    /**
     * Changes text and height in textarea
     * 
     * @param {object} e
     * @returns {Boolean}
     */
    function handleTextareaKeyup(e) {
        var text = this.value
        this.style.height = '1px'
        this.style.height = (this.scrollHeight - 4) + 'px'
        domain1.postMessage({
            action: 'renew',
            value: text
        }, domain)

        return true;
    }
})()
